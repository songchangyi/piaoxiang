package com.kgc.mapper;


import com.kgc.entity.Comment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommentMapper {
    //根据bookid查询评论
    List<Comment> queryByid(@Param("bookid") int bookid);
    //添加评论
    int add(@Param("comment") Comment comment);
    //删除评论
    int del(@Param("id") int id);
}
