package com.kgc.mapper;

import com.kgc.entity.Book;
import com.kgc.entity.BookType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookMapper {
    //查询所有图书
    List<Book> queryBook();
    //增加图书
    int addBook(@Param("book") Book book);
    //查询所有分类
    List<BookType> queryBookType();
    //通过id删除对应的记录
    int delBookById(@Param("id")int id);
    //通过id查询对应图书信息
    Book queryBookById(@Param("id")int id);
    //通过id更新该图书信息
    int updateBookById(@Param("book")Book book);
    //通过图书分类获取图书
    List<Book> queryBookByType(@Param("bookType")int bookType);
    //admin -- 按输入内容查询对应图书
    List<Book> queryBookBySearch(@Param("searchBook") String searchBook);
    //更改图书库存
    int updataBookNumber(@Param("bookid")Integer bookid,@Param("bookNumber")Integer bookNumber);



}
