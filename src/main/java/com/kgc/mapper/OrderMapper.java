package com.kgc.mapper;

import com.kgc.entity.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper {

    //增加一条订单信息
    int addOrder(@Param("order")Order order);
    //更新订单状态
    int updataOrderType(@Param("orderType")Integer orderType,@Param("ordernumber")String ordernumber);
    /**
     * 查询订单
     * @return
     */
    List<Order> queryOrder(@Param("userId") int userId);

    //通过订单编号查询订单
    List<Order> queryOrderByOrderNumber(@Param("orderNumber")String orderNumber);

    /**
     * 分类查询订单
     * @param userId
     * @param orderType
     * @return
     */
    List<Order> queryOrderByType(@Param("userId") int userId,@Param("orderType") int orderType);

    /**
     * 删除订单
     */
    int delOrder(@Param("id") int id);

}

