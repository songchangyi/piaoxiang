package com.kgc.service.impl;

import com.kgc.entity.Order;
import com.kgc.mapper.OrderMapper;
import com.kgc.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public int addOrder(Order order) {
        return orderMapper.addOrder(order);
    }

    @Override
    public int updataOrderType(Integer orderType,String ordernumber) {
        return orderMapper.updataOrderType(orderType,ordernumber);
    }

    @Override
    public List<Order> queryOrder(int userId) {

        return orderMapper.queryOrder(userId);
    }


    @Override
    public List<Order> queryOrderByType(int userId, int orderType) {
        return orderMapper.queryOrderByType(userId,orderType);
    }



    @Override
    public List<Order> queryOrderByOrderNumber(String OrderNumber) {
        return orderMapper.queryOrderByOrderNumber(OrderNumber);
    }

    @Override
    public int delOrder(int id) {
        return orderMapper.delOrder(id);
    }

}
