package com.kgc.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.kgc.entity.AlipayAttr;
import com.kgc.entity.Book;
import com.kgc.entity.Order;
import com.kgc.service.BookService;
import com.kgc.service.OrderService;
import com.kgc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("Alipay")
@ResponseBody
public class AlipayController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    @RequestMapping("to_alipay")
    protected void pay1(HttpServletRequest request,
                           HttpServletResponse response) throws ServletException, IOException {
        // 处理乱码
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        /*获得初始化的AlipayClient
         * AlipayClient alipayClient = new DefaultAlipayClient(
         * "https://openapi.alipaydev.com/gateway.do",
         * "2016091700535499","请复制第1步中生成的密钥中的商户应用私钥,
         * "json","utf-8","沙箱环境RSA2支付宝公钥","RSA2");
         * */
        AlipayClient alipayClient = new DefaultAlipayClient(
                AlipayAttr.gatewayUrl, AlipayAttr.app_id,
                AlipayAttr.merchant_private_key, "json", AlipayAttr.charset,
                AlipayAttr.alipay_public_key, AlipayAttr.sign_type);

        // 取购买人名称
        String in_name = request.getParameter("in_name");
        // 取手机号
        String in_phone = request.getParameter("in_phone");
        // 创建唯一订单号
        int random = (int) (Math.random() * 10000);
        String dateStr = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

        // 订单号拼接规则：手机号后四位+当前时间后四位+随机数四位数
        String out_trade_no = in_phone.substring(7) + dateStr.substring(10)
                + random;
        // 拼接订单名称
        String subject = in_name + "的订单";

        // 取付款金额
        String total_amount = request.getParameter("in_money");

        // 设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(AlipayAttr.return_url);//支付成功响应后跳转地址
        alipayRequest.setNotifyUrl(AlipayAttr.notify_url);//异步请求地址

        /*FAST_INSTANT_TRADE_PAY 二维码瞬时支付
         * out_trade_no 订单号 total_amount 订单金额  subject 订单名称
         */
        alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no
                + "\"," + "\"total_amount\":\"" + total_amount + "\","
                + "\"subject\":\"" + subject + "\"," + "\"body\":\""
                + ""+ "\"," + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        String result = "请求无响应";
        // 请求
        try {
            int userid = Integer.parseInt(request.getParameter("userid"));
            int bookid = Integer.parseInt(request.getParameter("bookid"));
            int bookNumber = Integer.parseInt(request.getParameter("bookNumber"));
            int numberAll = Integer.parseInt(request.getParameter("numberAll"));
            int addressid = Integer.parseInt(request.getParameter("addressid"));
            String ordertime = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            String ordernumber = out_trade_no;
            /* 1为已下单，未支付 */
            int orderType = 1;
            Order order = new Order();
            order.setUserId(userid);
            order.setBookId(bookid);
            order.setBookNumber(bookNumber);
            order.setOrderTime(ordertime);
            order.setOrderNumber(ordernumber);
            order.setOrderType(orderType);
            order.setAddressid(addressid);
            int num = orderService.addOrder(order);
            if (num == 1){
                System.out.println("添加订单成功!");
            }else {
                System.out.println("添加订单失败!");
            }
            /*减少库存*/
            bookService.updataBookNumber(bookid,(numberAll-bookNumber));

            //通过阿里客户端，发送支付页面请求
            result = alipayClient.pageExecute(alipayRequest).getBody();
            response.getWriter().println(result);
            response.getWriter().flush();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        } finally {
            response.getWriter().close();
        }

    }

    @RequestMapping("pay2")
    protected void pay2(HttpServletRequest request,
                           HttpServletResponse response) throws ServletException, IOException {
        // 处理乱码
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        /*获得初始化的AlipayClient
         * AlipayClient alipayClient = new DefaultAlipayClient(
         * "https://openapi.alipaydev.com/gateway.do",
         * "2016091700535499","请复制第1步中生成的密钥中的商户应用私钥,
         * "json","utf-8","沙箱环境RSA2支付宝公钥","RSA2");
         * */
        AlipayClient alipayClient = new DefaultAlipayClient(
                AlipayAttr.gatewayUrl, AlipayAttr.app_id,
                AlipayAttr.merchant_private_key, "json", AlipayAttr.charset,
                AlipayAttr.alipay_public_key, AlipayAttr.sign_type);

        String orderNumber =request.getParameter("orderNumber");
        List<Order> orders = orderService.queryOrderByOrderNumber(orderNumber);
        String out_trade_no = "";
        String subject = "";
        for (Order order : orders) {
            // 取购买人名称
            String in_name = order.getShippingAddress().getName();
            // 取手机号
            String in_phone = order.getShippingAddress().getPhone();

            // 拼接订单名称
            subject = in_name + "的订单";

        }
        // 取付款金额
        String total_amount = request.getParameter("in_money");

        // 设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(AlipayAttr.return_url);//支付成功响应后跳转地址
        alipayRequest.setNotifyUrl(AlipayAttr.notify_url);//异步请求地址

        /*FAST_INSTANT_TRADE_PAY 二维码瞬时支付
         * out_trade_no 订单号 total_amount 订单金额  subject 订单名称
         */
        alipayRequest.setBizContent("{\"out_trade_no\":\"" + orderNumber
                + "\"," + "\"total_amount\":\"" + total_amount + "\","
                + "\"subject\":\"" + subject + "\"," + "\"body\":\""
                + ""+ "\"," + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        String result = "请求无响应";
        // 请求
        try {
            //通过阿里客户端，发送支付页面请求
            result = alipayClient.pageExecute(alipayRequest).getBody();
            response.getWriter().println(result);
            response.getWriter().flush();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        } finally {
            response.getWriter().close();
        }

    }

    @RequestMapping("pay3")
    protected void pay3(HttpServletRequest request,
                        HttpServletResponse response) throws ServletException, IOException {
        // 处理乱码
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        /*获得初始化的AlipayClient
         * AlipayClient alipayClient = new DefaultAlipayClient(
         * "https://openapi.alipaydev.com/gateway.do",
         * "2016091700535499","请复制第1步中生成的密钥中的商户应用私钥,
         * "json","utf-8","沙箱环境RSA2支付宝公钥","RSA2");
         * */
        AlipayClient alipayClient = new DefaultAlipayClient(
                AlipayAttr.gatewayUrl, AlipayAttr.app_id,
                AlipayAttr.merchant_private_key, "json", AlipayAttr.charset,
                AlipayAttr.alipay_public_key, AlipayAttr.sign_type);

        //获取session里的Books集合
        List<Book> orderBooks = (List<Book>) request.getSession().getAttribute("orderBooks");

        // 取购买人名称
        String in_name = request.getParameter("in_name");
        // 取手机号
        String in_phone = request.getParameter("in_phone");
        // 创建唯一订单号
        int random = (int) (Math.random() * 10000);
        String dateStr = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

        // 订单号拼接规则：手机号后四位+当前时间后四位+随机数四位数
        String out_trade_no = in_phone.substring(7) + dateStr.substring(10)
                + random;
        // 拼接订单名称
        String subject = in_name + "的订单";

        // 取付款金额
        String total_amount = request.getParameter("in_money");

        // 设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(AlipayAttr.return_url);//支付成功响应后跳转地址
        alipayRequest.setNotifyUrl(AlipayAttr.notify_url);//异步请求地址

        /*FAST_INSTANT_TRADE_PAY 二维码瞬时支付
         * out_trade_no 订单号 total_amount 订单金额  subject 订单名称
         */
        alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no
                + "\"," + "\"total_amount\":\"" + total_amount + "\","
                + "\"subject\":\"" + subject + "\"," + "\"body\":\""
                + ""+ "\"," + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        String result = "请求无响应";
        // 请求
        try {
            for (Book book:orderBooks){
                int userid = Integer.parseInt(request.getParameter("userid"));
                int bookid = book.getId();
                int bookNumber = book.getNumber();

                int numberAll =  bookService.queryBookById(bookid).getNumber();
                int addressid = Integer.parseInt(request.getParameter("addressid"));
                String ordertime = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                String ordernumber = out_trade_no;
                /* 1为已下单，未支付 */
                int orderType = 1;
                Order order = new Order();
                order.setUserId(userid);
                order.setBookId(bookid);
                order.setBookNumber(bookNumber);
                order.setOrderTime(ordertime);
                order.setOrderNumber(ordernumber);
                order.setOrderType(orderType);
                order.setAddressid(addressid);
                int num = orderService.addOrder(order);
                if (num == 1){
                    System.out.println("添加订单成功!");
                }else {
                    System.out.println("添加订单失败!");
                }
                /*减少库存*/
                bookService.updataBookNumber(bookid,(numberAll-bookNumber));
                userService.delShopCarBook(userid,bookid);
            }
            request.getSession().removeAttribute("orderBooks");
            //通过阿里客户端，发送支付页面请求
            result = alipayClient.pageExecute(alipayRequest).getBody();
            response.getWriter().println(result);
            response.getWriter().flush();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        } finally {
            response.getWriter().close();
        }

    }
}
