package com.kgc.controller;


import com.kgc.entity.User;
import com.kgc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;

@Controller
@RequestMapping("file")
public class FileController {

    @Autowired
    private UserService userService;

    //入参就可以代表上传文件
    @RequestMapping("uploadhead")
    public String upload(int userid,@RequestParam("file") MultipartFile multipartFile, Model model, HttpServletRequest request) {
        //定义上传路径
        String uploadPath = request.getSession().getServletContext().getRealPath("/img/head") + File.separator;

        //1、传到哪里去  2、我传什么东西  3、传的细节

        //1、判断
        if (multipartFile != null && !multipartFile.isEmpty()) {
            //不空才传
            //2、获取原始文件名
            String originalFilename = multipartFile.getOriginalFilename();

            //3、 先截取原文件的文件名前缀，不带后缀
            String fileNamePrefix=originalFilename.substring(0,originalFilename.lastIndexOf("."));

            //4、加工处理文件名，将原文件+时间戳
            String newFileNamePrefix=fileNamePrefix+new Date().getTime();

            //5、得到新文件名
            String newFileName=newFileNamePrefix+originalFilename.substring(originalFilename.lastIndexOf("."));

            //6、构建文件对象
            File file=new File(uploadPath+newFileName);
            if(!file.exists()){
                file.mkdirs();
            }
            //7、上传
            try {
                multipartFile.transferTo(file);
                model.addAttribute("fileName",newFileName);
                userService.updateHead("../img/head/"+newFileName,userid);
                User user = userService.queryUserById(userid);
                request.getSession().setAttribute("user",user);
            }catch (IOException e){
                e.printStackTrace();
            }


        }

        return "pc/pc_detail";
    }
}