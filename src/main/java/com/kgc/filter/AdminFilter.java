package com.kgc.filter;


import com.kgc.entity.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AdminFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest res, ServletResponse req, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) res;
        HttpServletResponse response = (HttpServletResponse) req;
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        if (user == null) {
            response.sendRedirect(request.getContextPath()+"/login_register/login_register.jsp");
        }
        else if(user.getRoleid()!=1){
            // 没有登录
            response.sendRedirect(request.getContextPath()+"/index/main.jsp");
        }else{
            // 已经登录，继续请求下一级资源（继续访问）
            chain.doFilter(res, req);
        }

    }

    @Override
    public void destroy() {

    }
}
