<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8">
    <title>登录页</title>
    <link rel="stylesheet" href="../css/bootstrap.css"/>
    <link rel="stylesheet" href="../css/login.css"/>
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/login.js"></script>
</head>

<body>

<div class="from_div">
    <div class="form-inline">

        <div class="register-head">
            <div class="from_title">
                <h2>登录</h2></div>
            <div class="clearfix"></div>
            <span><a id="got_c">短信验证码登录</a><a id="got_p"  style="display: none">手机号密码登录</a></span>
            <div class="register">未有账号？
                <a href="register.jsp">请注册</a>
            </div>
        </div>

        <form id="t_plogin" action="#" method="get" >

            <div><input type="text" id="tel" name="tel" maxlength="11" placeholder="手机号"/></div>

            <div><input type="password" id="password" name="password" maxlength="12" placeholder="密码"/></div>
            <div class="auto-div">
                <div class="checkbox auto">
                    <label>
                        <input type="checkbox" id="autologin" name="autologin">下次自动登录
                    </label>
                </div>
                <div class="forget">
                    <a href='javascript:window.parent.location.href="forgetpassword.jsp"'>忘记密码</a>
                </div>
                <div class="clearfix"></div>

            </div>

            <div>
                <button type="button" id="submit">登录</button>
            </div>

        </form>
        <form id="t_clogin" action="#" method="get" style="display: none">

            <div><input type="text" id="ctel" name="ctel" maxlength="11" placeholder="手机号"/></div>

            <div><input type="text" id="lcode" name="password" maxlength="12" placeholder="验证码"/><button type="button" id="locode">获取验证码</button></div>

            <div>
                <button type="button" id="csubmit" style="margin-top: 30px">登录</button>
            </div>

        </form>

    </div>
</div>

</body>

</html>
