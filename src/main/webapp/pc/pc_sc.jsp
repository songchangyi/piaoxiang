<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8">
    <title></title>
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <style>
        * {
            margin: 0px;
            padding: 0px;
            font-size: 12px;
            color: #666666;
        }

        .sc_body {
            width: 700px;
        }

        .sc_book {
            padding: 20px;
            border: 1px solid gainsboro;
            margin: 10px;
        }

        .sc_img,
        .sc_detail {
            float: left
        }

        .sc_btn {
            margin-right: 50px;
            float: right;
        }

        .clear:after {
            display: block;
            content: "";
            clear: both;
        }

        .sc_img img {
            width: 45px;
            margin-right: 100px;
        }

        .sc_detail ul {
            list-style: none;
        }

        .sc_detail ul li:first-child {
            font-size: 14px;
            margin-bottom: 10px;
            color: black;
        }

        .sc_detail ul li:first-child span {
            font-size: 14px;
            color: black;
        }

        .sc_btn button {
            width: 80px;
            height: 30px;
            border: 2px solid gainsboro;
            margin-top: 15px;
        }

        .sc_nav {
            padding: 10px 0px 0px 20px;
            height: 10px;
            margin-bottom: 50px;
            font-size: 12px;
            color: #666666;
        }

        .nosc {
            width: 200px;
            margin: 100px auto;
            text-align: center;
        }

        .nosc a {
            color: #337AB7;
            text-decoration: none;
        }

        .nosc a:hover {
            color: orangered;
            cursor: pointer;
        }

        .sc_detail ul li:first-child a{
            font-size: 14px;
            text-decoration: none;
        }

        .sc_detail ul li:first-child span:hover,.sc_detail ul li:first-child a:hover{
            color: brown;
            text-decoration: underline;
            cursor: pointer;
        }
    </style>
</head>

<body>
<script>
    $(function () {
        var userid = $("#sc_userid").val()
        $.getJSON("../user/queryFavorite", {"userId": userid}, function (data1) {
            if (data1 == "") {
                var str = '<div class="nosc">您还没有收藏,<a href="javascript:window.parent.location.href=\'../index/main.jsp\'">点此前往首页</a></div>'
                $("#sc_body").append(str);
            } else {
                $(data1).each(function () {
                    $.getJSON("../book/queryBookById", {"id": this}, function (data2) {
                        var str
                            = " <div class=\"sc_book\">\n" +
                            "        <div class=\"sc_img\">\n" +
                            "            <img src='" + data2.picture + "'/>\n" +
                            "        </div>\n" +
                            "        <div class=\"sc_detail\">\n" +
                            "            <ul>\n" +
                            "                <li><a  href='javascript:window.parent.location.href=\"../index/bookInfo.jsp?id="+data2.id+"&number="+data2.number+"\"'>书名：<span>" + data2.name + "</span></a></li>\n" +
                            "                <li>作者：<span>" + data2.author + "</span></li>\n" +
                                "<li>价格：￥<span>" + data2.price + "</span></li>\n"+
                            "            </ul>\n" +
                            "        </div>\n" +
                            "        <div class=\"sc_btn\">\n" +
                            "            <div>\n" +
                            "                <button class='deletefavor'   type=\"button\">删除<input type='hidden' value='" + data2.id + "'></button>\n" +
                            "            </div>\n" +
                            "        </div>\n" +
                            "        <div class=\"clear\"></div>\n" +
                            "    </div>";

                        $("#sc_body").append(str);


                    })

                })


            }
            $("#sc_body").on("click",".deletefavor", function () {
                if (confirm("确认删除？")) {
                    var bookId = $(this).children("input").val();
                    $.getJSON("../user/delFavorite", {"userId": userid, "bookId": bookId}, function (data) {
                        if (data) {
                            window.location.href = "pc_sc.jsp";
                        } else {
                            alert("删除失败");
                        }
                    })
                }
            })
        })
    })

</script>
<input type="hidden" id="sc_userid" value="${user.id}">
<div class="sc_nav"> 个人中心>我的收藏</div>
<div class="sc_body" id="sc_body">


</div>
</body>

</html>
