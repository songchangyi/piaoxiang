<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.css"/>
    <link rel="stylesheet" href="../css/public_top.css"/>
    <link rel="stylesheet" href="../css/public_bottom.css"/>
    <link rel="icon" href="../img/favicon.ico" mce_href="../img/favicon.ico" type="image/x-icon">
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <style>
        body {
            background-color: #F5F5F4;
        }
    </style>
    <title>个人中心</title>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a href="#" class="navbar-brand glyphicon glyphicon-user">个人中心</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav  navbar-right">
                <li>
                    <a href="../index/main.jsp">首页</a>
                </li>
                <li>
                    <a href="../index/bookShopping.jsp">购物车</a>
                </li>
                <li>
                    <a href="../book/queryByType?pageNum=1&bookType=0">全部图书</a>
                </li>
                <li>
                    <a href="#">服务</a>
                </li>

            </ul>
        </div>
    </div>
</nav>

<div class="starter navbar-default">
    <div class="container">
        <div class="navbar-header col-md-4">
            <div class="navbar-brand tp">
                <img src="../img/LOGO/LOGO.png"/>
            </div>

        </div>
        <div class="col-md-6 col-md-offset-6">

            <div class="input-group col-md-8 sosuo">
                <input type="text" class="form-control" placeholder="请输入查询内容"/>
                <span class="input-group-btn">
						<button class="btn btn-danger">搜索</button>
						</span>
            </div>
            <div class="ss">
                <a href="#">诗词格律</a>
                <a href="#">园治</a>
                <a href="#">目知录</a>
                <a href="#">沧浪诗话</a>
                <a href="#">唐长孺</a>
            </div>
        </div>
    </div>
</div>

<div style="margin: 50px auto 0px;width: 950px;">
    <div style="width: 200px; margin-right: 30px;float: left;">
        <jsp:include page="pc_side.jsp"></jsp:include>

    </div>
    <div style="width: 720px;float: right;">
        <jsp:include page="pc_mid.jsp?"></jsp:include>
    </div>
    <div class="clearfix"></div>
</div>
<jsp:include page="../public/public_bottom.jsp"></jsp:include>

</body>

<script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../js/bootstrap.js"></script>
<script>


    $(function () {
        var ifurl = getUrlParam("ifurl");
        if (ifurl != null) {
            $("iframe").attr("src", ifurl+".jsp");
        }
    })

    function getUrlParam(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
        var r = window.location.search.substr(1).match(reg);  //匹配目标参数
        if (r != null) return unescape(r[2]);
        return null; //返回参数值
    }


</script>

</html>
