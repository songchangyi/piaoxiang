$(function () {

    var time = 60;

    $("#tel").blur(checktel);
    $("#password").blur(checkpwd);
    $("#autologin").change(function () {
        if ($("#autologin").prop("checked")) {
            alert("请在安全的网络环境下使用此功能！")
        }
    })

    function checkpwd() {
        rem($(this));
        var pwd = $(this).val();
        if (pwd == "") {
            add($(this));
            return false;
        }
        if (pwd.length < 6) {
            add($(this));
            return false;
        }

    }

    function checktel() {
        rem($(this));
        var tel = $(this).val();
        var reg = /^1[35789]\d{9}$/
        if (tel == "") {
            add($(this));
            return false;
        }
        if (!reg.test(tel)) {
            add($(this))
        }

    }

    function add(obj) {
        rem(obj);
        obj.after("<span class='glyphicon glyphicon-remove' style='color:darkred;'></span>");
    }

    function rem(obj) {
        obj.siblings("span").remove();
    }

    $("#submit").click(function () {
        var tel = $("#tel").val();
        var password = $("#password").val();
        var reg = /^1[35789]\d{9}$/
        if (tel == "" || password == "" || password.length < 6 || !reg.test(tel)) {
            return;
        }

        $.post("../user/login", {"tel": tel, "password": password}, function (data) {
            rem($("#password"));

            if (data == 1 || data == 2) {
                if ($("#autologin").prop("checked")) {
                    $.getJSON("../user/addCookie", {"key": "auto", "val": tel + "-" + password})
                }

                window.location.reload()

            } else if (data == 3) {
                add($("#password"), "用户名或密码错误!");
            }

        }, "json");
    })


    $("#got_c").click(function () {
        $("#got_c").css("display","none");
        $("#got_p").css("display","inline-block");
        $("#t_plogin").css("display","none");
        $("#t_clogin").css("display","block");
    })

    $("#got_p").click(function () {
        $("#got_p").css("display","none");
        $("#got_c").css("display","inline-block");
        $("#t_clogin").css("display","none");
        $("#t_plogin").css("display","block");
    })



    $("#locode").click(function () {

            var reg = /^1[35789]\d{9}$/;

            var phoneNo = $("#ctel").val();

            if (phoneNo == "" || phoneNo.length != 11||!reg.test(phoneNo)) {
                return;
            }

            $.getJSON("../user/isexisttel",{"tel":phoneNo},function (data) {
                if(!data){
                    alert("手机号未注册！请检查手机号或注册！")
                    return;
                }else {
                    $.post("../user/sendSms", {"phoneNo": phoneNo}, function (data) {
                        if (data) {
                            alert("验证码发送成功！");
                        } else {
                            alert("验证码发送失败！");
                        }
                    }, "json")
                    sixty();
                }
            })



    })

    function sixty() {

        if (time == 0) {
            time = 60;
            $("#locode").html("获取验证码");
            $("#locode").removeAttr("disabled");
            return;
        } else {
            time--;
            $('#locode').html("(" + time + "秒" + ")重发");
        }
        setTimeout(function () {
            sixty();
        }, 1000);

        $("#locode").attr("disabled", "disabled");
    }

    $("#csubmit").click(function () {
        var reg = /^1[35789]\d{9}$/;
        var phoneNo = $("#ctel").val();

        if (phoneNo == "" || phoneNo.length != 11||!reg.test(phoneNo)) {
            return;
        }

        var usercode = $("#lcode").val();

        $.post("../user/confirmup",{"usercode":usercode},function (data) {
            if (data) {
                $.post("../user/clogin", {"tel": phoneNo}, function (data1) {
                    if (data1){
                        window.location.reload();
                    }else {
                        alert("登录失败！")
                    }
                }, "json")
            }else {
                alert("验证码错误！")
            }
        },"json")


    })

})