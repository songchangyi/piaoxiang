/*从地址栏获取参数*/
function getUrlParam(key) {
    // 获取参数
    var url = window.location.search;
    // 正则筛选地址栏
    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
    // 匹配目标参数
    var result = url.substr(1).match(reg);
    //返回参数值
    return result ? decodeURIComponent(result[2]) : null;
}
var pn = 1;
var ps = 7;
/*获得地址栏传过来的需要搜索的内容*/
var searchBook = getUrlParam("text");
$(function () {
    SearchBook(pn,ps,searchBook);
    $("#search").click(function () {
        if ($("#searchBook").val().length != 0){
            searchBook = $("#searchBook").val();
        }
        $('#box ul').empty();
        $("#box").css("display","none");
        SearchBook(pn,ps,searchBook);
    })

    /*搜索框联想词*/
    /*判断下拉框是否显示*/
    $("#searchBook").focus(function () {
        if ($("#box ul").val().length != 0){
            $("#box").css("display","block");
        }
    })
    $("#searchBook").blur(function () {
        $("#box").css("display","block");
    })
    /*从后台查找模糊词并拼接*/
    $("#searchBook").keyup(function () {
        if ($("#searchBook").val().length != 0){
            $.getJSON("../book/admin_searchBook",{"pn":pn,"ps":ps,"searchBook":$("#searchBook").val()},function (data) {
                $('#box ul').empty();
                var obj = eval(data);
                if (data.firstPage != 0){
                    var str = "";
                    $(obj.list).each(function () {
                        str += "<li>"+this.name+"</li>";
                    })
                    $('#box ul').append(str);
                    $("#box").css("display","block");
                    var num = $('#box ul').children("li").length;

                    $('#box ul li').bind("click",function () {
                        $('#searchBook').val($(this).html());//写入input
                        $('#box ul li').remove();
                        $("#box").css("display","none");
                    })
                }
            })
        }else{
            $('#box ul').empty();
        }


    })
})

function SearchBook(pn,ps) {
    $.getJSON("../book/admin_searchBook",{"pn":pn,"ps":ps,"searchBook":searchBook},function (data) {
        $(".total").html(data.total);
        var str = "";
        if (data.firstPage == 0){
            $(".book-all").empty();
            str = "<div class=\"div_searchNull_img\">" +
                "<img src=\"../img/LOGO/serachNull.png\" />" +
                "</div>" +
                "<div class=\"div_searchNull\">" +
                "暂无搜索结果" +
                "</div>";
            $(".book-all").append(str);
        }else {
            var obj = eval(data);
            $(".book-all").empty();
            $(obj.list).each(function(){
                str += "<div class='row'>" +
                        "<div class='book-only book-first'>" +
                        "<div class='col-md-2'>" +
                        "<img src='"+this.picture+"'/>" +
                        "</div>" +
                        "<div class='col-md-8'>" +
                        "<ul class='book-ul-a'>" +
                        "<li>" +
                        "<a href='../index/bookInfo.jsp?id="+this.id+"&number="+this.number+"'>"+this.name+"</a>" +
                        "</li>" +
                        "<li>" +
                        this.author+" 著 / "+this.publish+" / "+this.publishDate+
                        "</li>" +
                        "</ul>" +
                        "</div>" +
                        "<div class='col-md-2'>" +
                        "<ul class='book-ul-b'>" +
                        "<li>价格：<span>"+this.price+"￥</span></li>" +
                        "<li>库存：<span>"+this.number+"</span>本</li>" +
                        "</ul>" +
                        "</div>" +
                        "<div class='clearfix'></div>" +
                        "</div>" +
                        "</div>"
            })
            $(".book-all").append(str);

            var page = "";
            page += "<nav aria-label=\"Page navigation\">" +
                "<ul class=\"pagination\">" +
                "<li><a href=\"javascript:void(0)\" onclick='SearchBook(1,"+ps+",searchBook)'>首页</a></li>";
            if(obj.hasPreviousPage){
                page += "<li>" +
                    "<a href=\"javascript:void(0)\" aria-label=\"Previous\" onclick=\"SearchBook("+(obj.pageNum-1)+","+ps+",searchBook)\">" +
                    "<span aria-hidden=\"true\">&laquo;</span>" +
                    "</a>" +
                    "</li>";
            }

            $(obj.navigatepageNums).each(function () {
                if(obj.pageNum==this){
                    page += "<li class='active'><a href=\"javascript:void(0)\" onclick='SearchBook("+this+","+ps+"),searchBook'>"+this+"</a></li>";
                }else{
                    page += "<li><a href=\"javascript:void(0)\" onclick='SearchBook("+this+","+ps+"),searchBook'>"+this+"</a></li>";
                }
            })

            if(obj.hasNextPage){
                page += "<li>\n" +
                    "<a href=\"javascript:void(0)\" aria-label=\"Next\" onclick='SearchBook("+(obj.pageNum+1)+","+ps+"),searchBook'>" +
                    "<span aria-hidden=\"true\">&raquo;</span>" +
                    "</a>" +
                    "</li>";
            }
            page += "<li><a href=\"javascript:void(0)\" onclick='SearchBook("+obj.pages+","+ps+",searchBook)'>未页</a></li>" +
                "</ul>" +
                "</nav>";
            $(".div_neiRong nav").empty();
            $(".div_neiRong nav").append(page);
        }
    })
}