<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>飘香书屋</title>
    <link rel="stylesheet" href="/css/rain.css">
    <link rel="icon" href="../img/favicon.ico" mce_href="../img/favicon.ico" type="image/x-icon">


</head>
<style>
    * {
        margin: 0px;
        padding: 0px;
    }

    body{

        background: url("img/LOGO/index_back.jpg") no-repeat  ;
        background-size: 1536px 864px;
    }

    .index_body {
        padding-top: 100px;
        width: 800px;
        margin: 0px auto;
    }

    .index_body div {
        width: 200px;
        float: left;
    }

    canvas {

        z-index: -1;
        position : absolute;
        display: block;
        margin: 0 auto;
    }

    .index_1,
    .index_2 {
        margin-right: 100px;
    }

    .index_body img {
        width: 200px;
        box-shadow: 10px 10px 10px rgba(0, 0, 0, .5);
        transition:transform 1s;
    }

    .index_body img:hover{
        transform: scale(1.1,1.1);

    }
</style>
<body>

<canvas id="rain"></canvas>

<div class="index_body">
    <div id="demo" class="index_1">
        <a href="index/main.jsp"><img src="img/LOGO/piao-xiang.png"></a>
    </div>
    <div  class="index_2">
        <a href="index/main.jsp"><img src="img/LOGO/home.png"></a>
    </div>
    <div  class="index_3">
        <a href="login_register/login_register.jsp"><img src="img/LOGO/login-register.png"></a>
    </div>
</div>



<script src="js/rain.js"></script>

</body>

</html>