<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/order.css" />
    <style>
        .button_save{
            background: #8C222C;
            color: white;
        }
        .button_save:hover{
            background: #7B111B;
            color: white;
        }
    </style>
    <title></title>
</head>

<body>
<%--<button class="btn btn-primary col-md-1 col-md-offset-5" data-target="#modal" data-toggle="modal">点击</button>--%>
<div class="modal fade " id="modal">
    <!-- 自适应 -->
    <div class="modal-dialog " style="margin-top: 100px; width: 650px;">
        <!--边框与阴影 -->
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">&times;</button>
                <h4>收货地址</h4>
            </div>
            <div class="divider"></div>
            <div class="modal-body">
                <table>
                    <tr>
                        <td><span class="span">*</span>选择所在地</td>
                        <td>
                            <div id="distpicker">
                                <select></select>
                                <select></select>
                                <select></select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="span">*</span>详细地址</td>
                        <td><input type="text" name="address" placeholder="请填写详细到门牌号,楼层及房间号的详细地址" /><span></span></td>
                    </tr>
                    <tr>
                        <td>邮政编码</td>
                        <td id="celect"><input type="text" name="youZhen" />输入详细地址后
                            <a href="#" onclick="you_bian();">邮局查询</a>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="span">*</span>收货人姓名</td>
                        <td><input type="text" name="username" id="username" /><span></span></td>
                    </tr>
                    <tr>
                        <td><span class="span">*</span>联系电话</td>
                        <td><input type="number" name="phone" placeholder="手机号" /><span></span></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div>
                                <button class="btn btn-lg btn_buy_1 button_save savaaddress">保存并使用</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="clearfix"></div>
            <div class="divider"></div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="modify">
    <!-- 自适应 -->
    <div class="modal-dialog " style="margin-top: 100px; width: 650px;">
        <!--边框与阴影 -->
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">&times;</button>
                <h4>收货地址</h4>
            </div>
            <div class="divider"></div>
            <div class="modal-body">
                <table>
                    <tr>
                        <td><span class="span">*</span>选择所在地</td>
                        <td>
                            <div id="distpicker1">
                                <select></select>
                                <select></select>
                                <select></select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="span">*</span>详细地址</td>
                        <td><input type="text" name="address1" style="width: 325px" placeholder="请填写详细到门牌号,楼层及房间号的详细地址" /><span></span></td>
                    </tr>
                    <tr>
                        <td>邮政编码</td>
                        <td id="celect1"><input type="text" name="youZhen1" />输入详细地址后
                            <a href="#" onclick="you_bian();">邮局查询</a>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="span">*</span>收货人姓名</td>
                        <td><input type="text" name="username1" id="username1" /><span></span></td>
                    </tr>
                    <tr>
                        <td><span class="span">*</span>联系电话</td>
                        <td><input type="number" name="phone1" placeholder="手机号" /><span></span></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div>
                                <button class="btn btn-lg btn_buy_1 button_save modifyaddress">保存并使用</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="clearfix"></div>
            <div class="divider"></div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../js/bootstrap.js"></script>
<script type="text/javascript" src="../js/distpicker.data.js"></script>
<script type="text/javascript" src="../js/distpicker.js"></script>
<script>
    $("#distpicker").distpicker();
    $("#distpicker1").distpicker();

    function you_bian() {
        window.open("http://cpdc.chinapost.com.cn/web/");
    }
    $(function() {
        /* 增加地址输入框判断 */
        $("input[name=address]").blur(function() {
            if($("input[name='address']").val() == "") {
                $("input[name='address']+span").html("<h5 class='triangle'></h5>请输入详细地址");
            } else {
                $("input[name='address']+span").html("");
            }
        })
        $("input[name=username]").blur(function() {
            if($("input[name='username']").val() == "") {
                $("input[name='username']+span").html("<h5 class='triangle'></h5>请输入姓名");
            } else {
                $("input[name='username']+span").html("");
            }
        })
        $("input[name=phone]").blur(function() {
            if($("input[name='phone']").val() == "") {
                $("input[name='phone']+span").html("<h5 class='triangle'></h5>请输入手机号");
            } else {
                $("input[name='phone']+span").html("");
            }
        })

        /* 修改地址输入框判断 */
        $("input[name=address1]").blur(function() {
            if($("input[name='address1']").val() == "") {
                $("input[name='address1']+span").html("<h5 class='triangle'></h5>请输入详细地址");
            } else {
                $("input[name='address1']+span").html("");
            }
        })
        $("input[name=username1]").blur(function() {
            if($("input[name='username1']").val() == "") {
                $("input[name='username1']+span").html("<h5 class='triangle'></h5>请输入姓名");
            } else {
                $("input[name='username1']+span").html("");
            }
        })
        $("input[name=phone1]").blur(function() {
            if($("input[name='phone1']").val() == "") {
                $("input[name='phone1']+span").html("<h5 class='triangle'></h5>请输入手机号");
            } else {
                $("input[name='phone1']+span").html("");
            }
        })
        /* 添加地址 */
        $(".savaaddress").click(function () {
            var reg = /^\d+$/;
            var reg1 = /^1[3456789]\d{9}$/;
            var address = $("#distpicker select option:selected").text()+ " " + ($(".modal-body input[name='address']").val()).replace(/\s*/g,"");
            var code = $(".modal-body input[name='youZhen']").val();
            var username = $(".modal-body input[name='username']").val();
            var phone = $(".modal-body input[name='phone']").val();
            if (code.length != 6 || !reg.test(code)){
                alert("请输入正确的邮编!")
            }else if (!reg1.test(phone)){
                alert("请输入正确的手机号")
            }else if ($(".modal-body input[name='address']").val() == ""){
                alert("请输入详细地址!")
            }else if (username == "") {
                alert("请输入姓名!")
            }else {
                $.getJSON("../address/addAddress",{"userid":userid,"address":address,"postCode":code,"name":username,"phone":phone},function (data) {

                    if (data){
                        alert("添加地址成功!")
                        window.location.reload();
                    }else{
                        alert("添加地址失败!")
                    }
                })
            }
        })

        $("button[class=close]").click(function () {
            window.location.reload();
        })


    })
</script>
</body>

</html>