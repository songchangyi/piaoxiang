<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="icon" href="../img/favicon.ico" mce_href="../img/favicon.ico" type="image/x-icon">
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript">
        $(function(){
            $.getJSON("../user/queryuserRole",{},function (data) {
                var str = "";
                $("select").empty();
                $(data).each(function () {
                    str+="<option value ='"+this.id+"' name='roleName'>"+this.roleName+"</option>"
                })
                $("select").append(str);
            })

            $("input[type=button]:first").click(function(){
                $.getJSON("../user/updateuser",$("form").serialize()+"&roleid="+$("option:selected").val(),function(data){
                    if(data){
                        alert("更新成功!");
                        window.location.href="admin.jsp";
                    }else{
                        alert("更新失败!");
                    }
                });
            })
            /*通过id查询对应的图书信息*/
            var id = getUrlParam("id");
            $.getJSON("../user/queryUserById", {"id": id}, function(data){
                $("input[name=id]").val(data.id);
                $("input[name=name]").val(data.name);
                $("input[name=password]").val(data.password);
                $("input[name=age]").val(data.age);
                $("input[name=sex]").val(data.sex);
                $("input[name=tel]").val(data.tel);
                $("input[name=birthday]").val(data.birthday);
                $("input[name=head]").val(data.head);
                $("option[value="+data.roleid+"]").attr("selected","selected");
            });
        })
        function getUrlParam(key) {
            // 获取参数
            var url = window.location.search;
            // 正则筛选地址栏
            var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
            // 匹配目标参数
            var result = url.substr(1).match(reg);
            //返回参数值
            return result ? decodeURIComponent(result[2]) : null;
        }
    </script>
    <title>增加用户</title>
    <style type="text/css">
        #addBooks {
            width: 666px;
            border-collapse: collapse;
            margin: 0 auto;
        }

        #addBooks td {
            border: 1px solid #333333;
            padding: 3px 7px;
        }

        #addBooks tr:nth-child(1) {
            height: 50px;
            background-color: #337AB7;
            color: white;
            text-align: center;
            font-size: 3em;
        }

        span {
            color: red;
        }

        input {
            width: 200px;
        }

        button {
            height: 20px;
            width: 50px;
            border: 1px solid #cccccc;
            border-radius: 5px;
            margin-bottom: 5px;
        }
        textarea{
            width: 200px;
            height: 100px;
        }
    </style>
</head>
<body>
<form >
    <table id="addBooks">
        <tr>
            <td colspan="8" class="bookHead">增加用户
                <input type="hidden" name="id" value="">
            </td>
        </tr>
        <tr>
            <td>用户名<span>(*)</span></td>
            <td><input type="text" name="name" id="name" value=""
                       required="required" /></td>
        </tr>
        <tr>
            <td>密码<span>(*)</span></td>
            <td><input type="text" name="password" id="password" value=""
                       required="required" /></td>
        </tr>
        <tr>
            <td>年龄<span>(*)</span></td>
            <td><input type="number" name="age" id="age" value=""
                       required="required" /></td>
        </tr>
        <tr>
            <td>性别<span>(*)</span></td>
            <td><input type="text" name="sex" id="sex" value=""
                       required="required" /></td>
        </tr>
        <tr>
            <td>手机号<span>(*)</span></td>
            <td><input type="number" name="tel" id="tel"
                       value="" required="required" /></td>
        </tr>
        <tr>
            <td>生日</td>
            <td><input type="date" name="birthday" id="birthday" value=""/></td>
        </tr>
        <tr>
            <td>头像<span>(*)</span></td>
            <td><input type="text" name="head" id="head" value="" required="required"/></td>
        </tr>
        <tr>
            <td>用户类型<span>(*)</span></td>
            <td>
                <select>

                </select>
            </td>
        </tr>
        <tr style="text-align: center;">
            <td colspan="2"><input type="button" value="提交">
                <input type="button"  onclick="window.history.back()" value="返回"></td>
        </tr>
    </table>
</form>
</body>
</html>
